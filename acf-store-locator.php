<?php
/*
Plugin Name: ACF Store Locator
Plugin URI: https://bitbucket.org/_karine/acf-store-locator
Description: Creates a store locator using ACF & ACF Location field (both required). Create stores and insert the store locator on a page with shortcode [acf-store-locator]
Version: 1.1
Author: Karine Do
Author URI: http://karine.do/
License: GPL
*/

class ACF_Store_Locator {

  protected static $version = '1.1';

  function __construct() {

    add_action( 'init', array( $this, 'init' ) );

    load_plugin_textdomain( 'acf-store-locator', false, basename( dirname( __FILE__ ) ) . '/languages' );

  }

  function is_acf_installed() {
    return class_exists('acf');
  }

  function is_acf_location_installed() {
    return class_exists('acf_field_location_plugin');
  }

  function init() {

    if ( $this->is_acf_installed() ) {

      // Admin styles
      add_action( 'admin_print_scripts',  array( $this, 'admin_enqueue_styles' ) );

      // Create Store post type
      $this->create_store_post_type();

      // Create ACF group
      $this->create_acf_group();

      // Add shortcode
      add_shortcode( 'acf-store-locator', array( $this, 'create_shortcode' ) );

    }

  }

  function admin_enqueue_styles() {

    // Store post type icon
    wp_enqueue_style('admin-menu-icons', plugins_url( 'admin-menu-icons.css', __FILE__), array(), self::$version, 'all');

  }

  function create_store_post_type() {

    $labels = array(
      'name' => __( 'Stores', 'acf-store-locator' ),
      'singular_name' => __( 'Store', 'acf-store-locator' ),
      'menu_name' => __( 'Stores', 'acf-store-locator' ),
      'all_items' => __( 'All Stores', 'acf-store-locator' ),
      'add_new' => __( 'Add New', 'acf-store-locator' ),
      'add_new_item' => __( 'Add new Store', 'acf-store-locator' ),
      'edit_item' => __( 'Edit Store', 'acf-store-locator' ),
      'new_item' => __( 'New Store', 'acf-store-locator' ),
      'view_item' => __( 'View Store', 'acf-store-locator' ),
      'search_items' => __( 'Search Store', 'acf-store-locator' ),
      'not_found' => __( 'No stores found', 'acf-store-locator' ),
      'not_found_in_trash' => __( 'No stores found in Trash', 'acf-store-locator' )
    );

    $args = apply_filters( 'acf_store_locator_args', array(
      'labels' => $labels,
      'public' => true,
      'exclude_from_search' => true,
      'publicly_queryable' => false,
      'show_ui' => true,
      'show_in_nav_menus' => false,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'menu_icon' => 'dashicons-location-alt',
      'has_archive' => false,
      'query_var' => false,
      'can_export' => true,
      'menu_position' => 5
    ) );

    register_post_type( 'acf-store', $args );

  }

  function create_acf_group() {

    register_field_group(array (
      'id' => 'acf_store-location',
      'title' => __('Store location', 'acf-store-locator'),
      'fields' => array (
        array (
          'key' => 'acf_store_location',
          'label' => __('Location', 'acf-store-locator'),
          'name' => 'location',
          'type' => 'google_map',
          'required' => 1,
          'val' => 'address',
          'mapheight' => 300,
          'center' => '48.856614,2.3522219000000177',
          'zoom' => 10,
          'scrollwheel' => 1,
          'mapTypeControl' => 1,
          'streetViewControl' => 1,
          'PointOfInterest' => 1,
        ),
        array (
          'key' => 'acf_store_street',
          'label' => __('Street Address', 'acf-store-locator'),
          'name' => 'street_address',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => '',
        ),
        array (
          'key' => 'acf_store_city',
          'label' => __('City', 'acf-store-locator'),
          'name' => 'city',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => '',
        ),
        array (
          'key' => 'acf_store_state',
          'label' => __('State', 'acf-store-locator'),
          'name' => 'state',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => '',
        ),
        array (
          'key' => 'acf_store_zip',
          'label' => __('Zip Code', 'acf-store-locator'),
          'name' => 'zip_code',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => '',
        ),
        array (
          'key' => 'acf_store_phone',
          'label' => __('Phone', 'acf-store-locator'),
          'name' => 'phone',
          'type' => 'text',
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'formatting' => 'none',
          'maxlength' => '',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'acf-store',
            'order_no' => 0,
            'group_no' => 0,
          ),
        ),
      ),
      'options' => array (
        'position' => 'normal',
        'layout' => 'no_box',
        'hide_on_screen' => array (
        ),
      ),
      'menu_order' => 0,
    ));

  }

  function create_shortcode( $atts ) {
    $atts = extract( shortcode_atts( array( '' => '' ), $atts ) );

    $lang = get_locale();

    if ( preg_match('/(\S{2})_(\S{2})/i', $lang, $match) ) {
      $language = $match[1];
      $region = $match[2];
    }

    // Enqueue necessary scripts and styles
    wp_enqueue_script( 'bootstrap-typeahead', plugins_url('js/bootstrap.min.js', __FILE__), array('jquery'), self::$version, true );
    wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?sensor=false&v=3.exp&language=' . $language . '&region=' . $region, '', self::$version, false );
    wp_enqueue_script( 'acf-store-locator-js', plugins_url('js/acf-store-locator.js', __FILE__), array('jquery', 'google-maps'), self::$version, true );
    wp_enqueue_style( 'acf-store-locator-css',plugins_url('css/acf-store-locator.css', __FILE__) );

    // Get all stores
    $args = array(
      'post_type' => 'acf-store',
      'posts_per_page' => -1
      );

    $storeslist = new WP_Query( $args );

    $store_data = array();
    $stores = array();

    if ( $storeslist->have_posts() ) {
      while ( $storeslist->have_posts() ) {
        $storeslist->the_post();

        $title = get_the_title();
        $description = get_the_content();
        $location = get_field('location');
        if ( current_theme_supports('post-thumbnails') ) {
          $image = get_the_post_thumbnail( get_the_ID(), 'thumbnail', array('class' => 'alignleft') );
        } else {
          $image = '';
        }
        $address = get_field('street_address');
        $city = get_field('city');
        $state = get_field('state');
        $zip_code = get_field('zip_code');
        $phone = get_field('phone');

        $telephone = ($phone ? '<p>' . __('Phone', 'acf-store-locator') . ': ' . $phone : '</p>');
        $directions = urlencode($address . ' ' . $city . ' ' . $state . ' ' . $zip_code);
        $directions_text = __('Directions', 'acf-store-locator');

        $lat = $location['lat'];
        $long = $location['lng'];

$infowindow = <<<EOD
<div class="store-info">$image<h5>$title</h5><p>$address $city $state $zip_code</p>$telephone<p><small><a href="https://maps.google.com/maps?daddr=$directions" target="_blank">$directions_text</a></small></p>$description</div>
EOD;

        $store_data[] = $city;
        $store_data[] = $zip_code;

        $store = array($title, $lat, $long, $infowindow);
        $stores[] = $store;
      }
    }
    wp_reset_postdata();

    $store_data = array_unique($store_data);
    $store_source = json_encode(array_values($store_data));
    $stores = json_encode($stores);

    $store_cityorzip = __('City or Zip', 'acf-store-locator');
    $store_findstore = __('Find Store', 'acf-store-locator');
    $store_error = __('Error', 'acf-store-locator');
    $store_errormessage = __('Please enter a city or zip code.', 'acf-store-locator');

$store_locator_html = <<<EOD
<script type="text/javascript">
  var shops = $stores;
</script>
<div id="acf-store-locator">
<div id="acf-form">
  <div class="alignleft">
    <input autocomplete="off" id="acf-address" class="form-control" type="text" placeholder="$store_cityorzip" onkeypress="doSearch(event);" data-provide="typeahead" data-items="4" data-source='$store_source'>
  </div>
  <div class="alignright">
    <button id="acf-search-store" type="button" onclick="codeAddress()" class="btn">$store_findstore</button>
  </div>
</div>

<div id="acf-error" class="alert alert-error">
  <h4>$store_error</h4>
  <p>$store_errormessage</p>
</div>
<div id="acf-map-canvas"></div>
</div>
EOD;

    return $store_locator_html;

  }

}

$acf_store_locator = new ACF_Store_Locator();